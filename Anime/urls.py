from django.conf.urls import patterns, include, url
from AnimeManager.views import Search, ParseShow
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'Anime.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^search/(?P<searchData>\w{0,50})/$', Search.as_view()),
    url(r'^add/(?P<showID>\d+)/$', ParseShow.as_view(), name="ParseShow"),
)
