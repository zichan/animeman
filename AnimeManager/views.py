import xml.etree.ElementTree as ET
import urllib2
import zipfile
from StringIO import StringIO

from django.db import transaction, IntegrityError
from django.views.generic import TemplateView

from AnimeManager.models import Anime, Episode, Genre

# Create your views here.

API_KEY = '80AEDB85A5421189'


class Search(TemplateView):
    template_name = 'search.html'

    TV_SERVER_TIME_URL = "http://thetvdb.com/api/Updates.php?type=none"
    TV_SEARCH_TITLE = "http://thetvdb.com/api/GetSeries.php?seriesname=%s"
    SeriesData = []

    def getTVTime(self):
        root = ET.parse(urllib2.urlopen(self.TV_SERVER_TIME_URL))

        for time in root.findall("Time"):
            self.TV_SERVER_TIME = time.text

    def getAnimeID(self):
        root = ET.parse(urllib2.urlopen(self.TV_SEARCH_TITLE % self.kwargs['searchData']))
        print(self.SeriesData)
        tmpList = []
        for Series in root.findall("Series"):
            tmp = {}
            tmp['id'] = Series.find('seriesid').text
            tmp['SeriesName'] = Series.find('SeriesName').text
            if Series.find('banner') != None:
                tmp['banner'] = 'http://thetvdb.com/banners/' + Series.find('banner').text

            list.append(tmpList, tmp)
        return tmpList

    def get_context_data(self, **kwargs):
        context = super(Search, self).get_context_data(**kwargs)
        self.SeriesData = []
        context['SeriesData'] = self.getAnimeID()
        return context


class ParseShow(TemplateView):
    template_name = 'parse.html'
    TEMP_XML_DATA = ''
    TV_ZIP_URL = "http://thetvdb.com/api/{0}/series/{1}/all/en.zip"
    TV_SERVER_TIME_URL = "http://thetvdb.com/api/Updates.php?type=none"
    ShowDataZip = ''

    def get_context_data(self, **kwargs):
        context = super(ParseShow, self).get_context_data(**kwargs)
        self.getZip()
        context['SeriesInfo'] = self.addShow()
        return context

    def addShow(self):
        try:
            with transaction.atomic():
                SeriesInfo = self.pullLangXML()
                Seriestag = SeriesInfo[0]["Series"]
                newShow = Anime()

                newShow.Title = Seriestag['SeriesName']
                newShow.Summary = Seriestag['Overview']
                newShow.lastUpdated = Seriestag['lastupdated']
                newShow.TVID = Seriestag['id']
                newShow.save()
                for episode in SeriesInfo[1:]:
                    tmp = Episode()
                    tmp.Title = episode["Episode"]["EpisodeName"]
                    tmp.Synopsis = episode['Episode']['Overview']
                    tmp.TVEpisodeID = episode['Episode']['id']
                    tmp.AnimeID = newShow
                    tmp.save()
                for genre in Seriestag["Genre"].split("|")[1:]:
                    try:
                        tmp = Genre.objects.get(Title=genre)
                    except:
                        tmp = Genre(Title=genre)
                        tmp.save()
                    newShow.Genres.add(tmp)
                newShow.save()
            return SeriesInfo
        except IntegrityError as e:
            return "Error Adding to Database" + e.message

    def getTVTime(self):
        root = ET.parse(urllib2.urlopen(self.TV_SERVER_TIME_URL))

        for time in root.findall("Time"):
            TV_SERVER_TIME = time.text
        return TV_SERVER_TIME

    def getZip(self):
        self.ShowDataZip = zipfile.ZipFile(
            StringIO(urllib2.urlopen(self.TV_ZIP_URL.format(API_KEY, self.kwargs['showID'])).read()))

    def pullLangXML(self):
        self.SeriesInfo = []
        root = ET.fromstring(self.ShowDataZip.open('en.xml').read())
        for element in root:
            list.append(self.SeriesInfo, etree_to_dict(element))

        return self.SeriesInfo


from collections import defaultdict
#Found at http://stackoverflow.com/a/10076823


def etree_to_dict(t):
    d = {t.tag: {} if t.attrib else None}
    children = list(t)
    if children:
        dd = defaultdict(list)
        for dc in map(etree_to_dict, children):
            for k, v in dc.iteritems():
                dd[k].append(v)
        d = {t.tag: {k: v[0] if len(v) == 1 else v for k, v in dd.iteritems()}}
    if t.attrib:
        d[t.tag].update(('@' + k, v) for k, v in t.attrib.iteritems())
    if t.text:
        text = t.text.strip()
        if children or t.attrib:
            if text:
                d[t.tag]['#text'] = text
        else:
            d[t.tag] = text
    return d