from django.contrib import admin
from AnimeManager.models import Anime, Episode, Genre
# Register your models here.

admin.site.register(Anime, admin.ModelAdmin)
admin.site.register(Episode, admin.ModelAdmin)
admin.site.register(Genre, admin.ModelAdmin)