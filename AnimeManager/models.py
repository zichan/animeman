from django.db import models
from django.contrib.auth.models import User
# Create your models here.
#class Profile(models.Model):
    #user = models.OneToOneField(User)
    #addedAnime = models.ForeignKey(Anime)


class Genre(models.Model):
    Title = models.TextField(default="", null=False, unique=True)

    def __unicode__(self):
        return self.Title


class Anime(models.Model):
    Title = models.TextField(default="", null=False)
    Banner = models.ImageField(upload_to="images/banners")
    TVID = models.IntegerField()
    Genres = models.ManyToManyField(Genre)
    Summary = models.TextField()
    lastUpdated = models.IntegerField()

    def __unicode__(self):
        return self.Title


class Episode(models.Model):
    Title = models.TextField(default="")
    Synopsis = models.TextField(null=True)
    TVEpisodeID = models.IntegerField()
    AnimeID = models.ForeignKey(Anime)

    def __unicode__(self):
        return self.Title
